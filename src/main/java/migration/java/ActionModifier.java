package migration.java;

import java.io.File;
import java.util.Arrays;

import org.apache.commons.lang3.tuple.Pair;

import migration.RegexpModifier;

public class ActionModifier extends RegexpModifier{
	public static Pair<String, String>[] REPLACEMENTS = new Pair[] {
			// Pair.<String, String>of("import com\\.avaje\\.ebean\\.event\\.", "import io.ebean.event."),
			Pair.<String, String>of("import play\\.libs\\.F\\;", "import java.util.concurrent.*;"),
			Pair.<String, String>of("import play\\.libs\\.F\\.Function\\;", "// import play.libs.F.Function;"),
			Pair.<String, String>of("F\\.Promise\\<Result\\> call\\(Http\\.Context context\\)", "CompletionStage<Result> call(play.mvc.Http.Context context)"),
			Pair.<String, String>of("F\\.Promise\\<Result\\> call\\(Context context\\)", "CompletionStage<Result> call(play.mvc.Http.Context context)"),
			Pair.<String, String>of("return F\\.Promise\\.promise", "return CompletableFuture.<Result>supplyAsync")
	};
		
	public ActionModifier() {
		super(Arrays.asList(REPLACEMENTS));
	}


	@Override
	public boolean isApplied(File file) {
		return file.getName().endsWith(".java") && !file.getName().equals("ObjectHelper.java") && !file.getName().equals("ChatController.java");
	}
}
