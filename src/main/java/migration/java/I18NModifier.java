package migration.java;

import java.io.File;
import java.util.Arrays;

import org.apache.commons.lang3.tuple.Pair;

import migration.RegexpModifier;

public class I18NModifier extends RegexpModifier{
	public static Pair<String, String>[] REPLACEMENTS = new Pair[] {
			// Pair.<String, String>of("import com\\.avaje\\.ebean\\.event\\.", "import io.ebean.event."),
			Pair.<String, String>of("import play\\.api\\.i18n\\.Lang;", "import play.i18n.Lang;"),
			Pair.<String, String>of("enLocale\\.getLanguage\\(\\), enLocale\\.getCountry\\(\\)", "enLocale")
		};
		
		public I18NModifier() {
			super(Arrays.asList(REPLACEMENTS));
		}

		@Override
		public boolean isApplied(File file) {
			return file.getName().endsWith(".java");
		}
}
