package migration.java;

import java.io.File;
import java.util.Arrays;

import org.apache.commons.lang3.tuple.Pair;

import migration.RegexpModifier;

public class WebSocketModifier extends RegexpModifier{
	public static Pair<String, String>[] REPLACEMENTS = new Pair[] {
			Pair.<String, String>of("WebSocket([^A-Za-z])(?!reject)", "LegacyWebSocket$1"),
			Pair.<String, String>of("WebSocket.reject", "play.mvc.WebSocket.reject"),
		};
		
		public WebSocketModifier() {
			super(Arrays.asList(REPLACEMENTS));
		}

		@Override
		public boolean isApplied(File file) {
			return file.getName().endsWith(".java");
		}
}
