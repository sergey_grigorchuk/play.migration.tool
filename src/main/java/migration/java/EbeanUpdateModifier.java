package migration.java;

import java.io.File;
import java.util.Arrays;

import org.apache.commons.lang3.tuple.Pair;

import migration.RegexpModifier;

public class EbeanUpdateModifier extends RegexpModifier {

	public static Pair<String, String>[] REPLACEMENTS = new Pair[] {
			Pair.<String, String>of("Ebean\\.update\\(", "services.core.EbeanService.update("),
	};
		
	public EbeanUpdateModifier() {
		super(Arrays.asList(REPLACEMENTS));
	}

	@Override
	public boolean isApplied(File file) {
		return file.getName().endsWith(".java");
	}

}
