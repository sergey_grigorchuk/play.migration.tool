package migration.java;

import java.io.File;
import java.util.Arrays;

import org.apache.commons.lang3.tuple.Pair;

import migration.RegexpModifier;

public class CallControllerStaticModifier extends RegexpModifier {

	public static Pair<String, String>[] REPLACEMENTS = new Pair[] {
			Pair.<String, String>of(" AssetsController\\.at", " (/*TODO play25 migration*/(AssetsController)null).at"),
			Pair.<String, String>of(" Assets\\.at", " (/*TODO play25 migration*/(Assets)null).at"),
			Pair.<String, String>of(" controllers\\.Assets\\.at", " (/*TODO play25 migration*/(controllers.Assets)null).at"),
			Pair.<String, String>of(" LoginController\\.securePasswords", " (/*TODO play25 migration*/(LoginController)null).securePasswords"),
			Pair.<String, String>of(" FileController\\.getFile", " (/*TODO play25 migration*/(FileController)null).getFile"),
			Pair.<String, String>of(" FileController\\.innerGetFile", " (/*TODO play25 migration*/(FileController)null).innerGetFile"),
			Pair.<String, String>of(" OrderController\\.orderInnerCreate", " (/*TODO play25 migration*/(OrderController)null).orderInnerCreate"),
			Pair.<String, String>of(" OrderController\\.getOffersComparePage", " (/*TODO play25 migration*/(OrderController)null).getOffersComparePage"),
			Pair.<String, String>of(" OrderController\\.getOrderItemsJson", " (/*TODO play25 migration*/(OrderController)null).getOrderItemsJson"),
			Pair.<String, String>of(" OrderController\\.downloadByName", " (/*TODO play25 migration*/(OrderController)null).downloadByName"),
			Pair.<String, String>of(" OrderController\\.removeFile", " (/*TODO play25 migration*/(OrderController)null).removeFile"),
			Pair.<String, String>of(" OrderController\\.saveOrder", " (/*TODO play25 migration*/(OrderController)null).saveOrder"),
			Pair.<String, String>of(" controllers\\.core\\.OrderController\\.saveOrder", " (/*TODO play25 migration*/(controllers.core.OrderController)null).saveOrder"),
			Pair.<String, String>of(" OrderController\\.uploadFile", " (/*TODO play25 migration*/(OrderController)null).uploadFile"),
			Pair.<String, String>of(" OrderController\\.changeOrderActuality", " (/*TODO play25 migration*/(OrderController)null).changeOrderActuality"),
			
			Pair.<String, String>of(" OrderController\\.sendReRequestsJson", " (/*TODO play25 migration*/(OrderController)null).sendReRequestsJson"),
			Pair.<String, String>of(" OrderController\\.returnToEdit", " (/*TODO play25 migration*/(OrderController)null).returnToEdit"),
			Pair.<String, String>of(" OrderController\\.acceptOffer", " (/*TODO play25 migration*/(OrderController)null).acceptOffer"),
			Pair.<String, String>of(" OrderController\\.disacceptOffer", " (/*TODO play25 migration*/(OrderController)null).disacceptOffer"),
			Pair.<String, String>of(" OrderController\\.download", " (/*TODO play25 migration*/(OrderController)null).download"),
			Pair.<String, String>of(" OrderController\\.downloadByName", " (/*TODO play25 migration*/(OrderController)null).downloadByName"),
			
			Pair.<String, String>of(" OrderController\\.createOrderFromExternalSystem", " (/*TODO play25 migration*/(OrderController)null).createOrderFromExternalSystem"),
			// Pair.<String, String>of(" OrderController\\.", " (/*TODO play25 migration*/(OrderController)null)."),
			
			Pair.<String, String>of(" WorkOrderController\\.setInspectionForWorkOrders", " (/*TODO play25 migration*/(WorkOrderController)null).setInspectionForWorkOrders"),
			Pair.<String, String>of(" WorkOrderController\\.setPlanReportMonthForWorkOrders", " (/*TODO play25 migration*/(WorkOrderController)null).setPlanReportMonthForWorkOrders"),
			Pair.<String, String>of(" WorkOrderController\\.getMounterWorkEvents", " (/*TODO play25 migration*/(WorkOrderController)null).getMounterWorkEvents"),
			Pair.<String, String>of(" WorkOrderController\\.getMounterWorkEventPhoto", " (/*TODO play25 migration*/(WorkOrderController)null).getMounterWorkEventPhoto"),
			Pair.<String, String>of(" WorkOrderController\\.checkMounterEnterEvent", " (/*TODO play25 migration*/(WorkOrderController)null).checkMounterEnterEvent"),
			Pair.<String, String>of(" WorkOrderController\\.checkMounterLeaveEvent", " (/*TODO play25 migration*/(WorkOrderController)null).checkMounterLeaveEvent"),
			Pair.<String, String>of(" WorkOrderController\\.exportMounterWorkTimeTable", " (/*TODO play25 migration*/(WorkOrderController)null).exportMounterWorkTimeTable"),
			Pair.<String, String>of(" WorkOrderController\\.exportMountersWorkTimeTable", " (/*TODO play25 migration*/(WorkOrderController)null).exportMountersWorkTimeTable"),
			Pair.<String, String>of(" WorkOrderController\\.getMounterHourRate", " (/*TODO play25 migration*/(WorkOrderController)null).getMounterHourRate"),
			Pair.<String, String>of(" WorkOrderController\\.downloadDataSourceForReport", " (/*TODO play25 migration*/(WorkOrderController)null).downloadDataSourceForReport"),
			// Pair.<String, String>of(" WorkOrderController\\.", " (/*TODO play25 migration*/(WorkOrderController)null)."),

			Pair.<String, String>of(" OfferController\\.acceptOffer", " (/*TODO play25 migration*/(OfferController)null).acceptOffer"),
			Pair.<String, String>of(" OfferController\\.disacceptOffer", " (/*TODO play25 migration*/(OfferController)null).disacceptOffer"),
			Pair.<String, String>of(" OfferController\\.editOffer", " (/*TODO play25 migration*/(OfferController)null).disacceptOffer"),
			Pair.<String, String>of(" OfferController\\.getOfferImage", " (/*TODO play25 migration*/(OfferController)null).getOfferImage"),
			Pair.<String, String>of(" OfferController\\.innerSaveOffer", " (/*TODO play25 migration*/(OfferController)null).innerSaveOffer"),
			Pair.<String, String>of(" OfferController\\.reconvertImages", " (/*TODO play25 migration*/(OfferController)null).reconvertImages"),

			Pair.<String, String>of(" OfferController\\.getOffersFor1CJson", " (/*TODO play25 migration*/(OfferController)null).getOffersFor1CJson"),
			Pair.<String, String>of(" OfferController\\.removeOffer", " (/*TODO play25 migration*/(OfferController)null).removeOffer"),
			Pair.<String, String>of(" OfferController\\.updateOfferDocumentEdiStateByCustomer", " (/*TODO play25 migration*/(OfferController)null).updateOfferDocumentEdiStateByCustomer"),
			Pair.<String, String>of(" OfferController\\.getEdiStatesForCustomer", " (/*TODO play25 migration*/(OfferController)null).getEdiStatesForCustomer"),
			Pair.<String, String>of(" OfferController\\.checkOfferAccepts", " (/*TODO play25 migration*/(OfferController)null).checkOfferAccepts"),
			Pair.<String, String>of(" OfferController\\.getOffersOverallAmount", " (/*TODO play25 migration*/(OfferController)null).getOffersOverallAmount"),
			Pair.<String, String>of(" OfferController\\.getOfferStatesJson", " (/*TODO play25 migration*/(OfferController)null).getOfferStatesJson"),
			Pair.<String, String>of(" OfferController\\.getEdiStateStatuses", " (/*TODO play25 migration*/(OfferController)null).getEdiStateStatuses"),
			Pair.<String, String>of(" OfferController\\.getOfferDocumentTypes", " (/*TODO play25 migration*/(OfferController)null).getOfferDocumentTypes"),
			Pair.<String, String>of(" OfferController\\.getEdiStateSignatureStatuses", " (/*TODO play25 migration*/(OfferController)null).getEdiStateSignatureStatuses"),
			Pair.<String, String>of(" OfferController\\.getEdiStateRevocationStatuses", " (/*TODO play25 migration*/(OfferController)null).getEdiStateRevocationStatuses"),
			Pair.<String, String>of(" OfferController\\.getEdiStateResolutionStatuses", " (/*TODO play25 migration*/(OfferController)null).getEdiStateResolutionStatuses"),
			Pair.<String, String>of(" OfferController\\.getOfferPayersJson", " (/*TODO play25 migration*/(OfferController)null).getOfferPayersJson"),
			Pair.<String, String>of(" OfferController\\.getOfferAcceptorsJson", " (/*TODO play25 migration*/(OfferController)null).getOfferAcceptorsJson"),
			Pair.<String, String>of(" OfferController\\.getEdiStateCustomersForCustomer", " (/*TODO play25 migration*/(OfferController)null).getEdiStateCustomersForCustomer"),
			Pair.<String, String>of(" OfferController\\.getEdiStateSuppliersForCustomer", " (/*TODO play25 migration*/(OfferController)null).getEdiStateSuppliersForCustomer"),
			Pair.<String, String>of(" OfferController\\.createOfferDocumentBySupplier", " (/*TODO play25 migration*/(OfferController)null).createOfferDocumentBySupplier"),
			Pair.<String, String>of(" OfferController\\.createOfferDocumentEdiStateBySupplier", " (/*TODO play25 migration*/(OfferController)null).createOfferDocumentEdiStateBySupplier"),
			Pair.<String, String>of(" OfferController\\.getEdiStatesForSupplier", " (/*TODO play25 migration*/(OfferController)null).getEdiStatesForSupplier"),
			
			Pair.<String, String>of(" OfferController\\.getOfferSuppliersJson", " (/*TODO play25 migration*/(OfferController)null).getOfferSuppliersJson"),
			Pair.<String, String>of(" OfferController\\.getOfferDeleteReasons", " (/*TODO play25 migration*/(OfferController)null).getOfferDeleteReasons"),
			Pair.<String, String>of(" OfferController\\.getEdiStateCustomersForSupplier", " (/*TODO play25 migration*/(OfferController)null).getEdiStateCustomersForSupplier"),
			Pair.<String, String>of(" OfferController\\.getEdiStateSuppliersForSupplier", " (/*TODO play25 migration*/(OfferController)null).getEdiStateSuppliersForSupplier"),
			Pair.<String, String>of(" OfferController\\.updateOfferDocumentEdiStateBySupplier", " (/*TODO play25 migration*/(OfferController)null).updateOfferDocumentEdiStateBySupplier"),
			
			
			Pair.<String, String>of(" SupplierController\\.createOrderInWork", " (/*TODO play25 migration*/(SupplierController)null).createOrderInWork"),
			Pair.<String, String>of(" SupplierController\\.getOffersDeviationForOrderJson", " (/*TODO play25 migration*/(SupplierController)null).getOffersDeviationForOrderJson"),
			Pair.<String, String>of(" SupplierController\\.getOrdersForSupplierPagedJson", " (/*TODO play25 migration*/(SupplierController)null).getOrdersForSupplierPagedJson"),
			Pair.<String, String>of(" SupplierController\\.createOfferDocumentBySupplier", " (/*TODO play25 migration*/(SupplierController)null).createOfferDocumentBySupplier"),
			Pair.<String, String>of(" SupplierController\\.createOfferFrom1C", " (/*TODO play25 migration*/(SupplierController)null).createOfferFrom1C"),
			Pair.<String, String>of(" SupplierController\\.getOrdersForSupplierJson", " (/*TODO play25 migration*/(SupplierController)null).getOrdersForSupplierJson"),
			
			Pair.<String, String>of(" ApiController\\.checkToken", " (/*TODO play25 migration*/(ApiController)null).checkToken"),

			Pair.<String, String>of(" ProjectApiController\\.getProjectsJson", " (/*TODO play25 migration*/(ProjectApiController)null).getProjectsJson"),
			Pair.<String, String>of(" ProjectApiController\\.getProjectSystemNamesJson", " (/*TODO play25 migration*/(ProjectApiController)null).getProjectSystemNamesJson"),
			Pair.<String, String>of(" ProjectController\\.createProjectApi", " (/*TODO play25 migration*/(ProjectController)null).createProjectApi"),
			Pair.<String, String>of(" CostItemController\\.getCostItems", " (/*TODO play25 migration*/(CostItemController)null).getCostItems"),
			Pair.<String, String>of(" CategoryController\\.getCategoriesJson", " (/*TODO play25 migration*/(CategoryController)null).getCategoriesJson"),
			Pair.<String, String>of(" UserController\\.getColleagues", " (/*TODO play25 migration*/(UserController)null).getColleagues"),
			Pair.<String, String>of(" CurrencyController\\.getCurrencies", " (/*TODO play25 migration*/(CurrencyController)null).getCurrencies"),
			Pair.<String, String>of(" WorkOrderController\\.getWorkTypesJson", " (/*TODO play25 migration*/(WorkOrderController)null).getWorkTypesJson"),
			

			Pair.<String, String>of(" InviteController\\.applyInvite", " (/*TODO play25 migration*/(InviteController)null).applyInvite"),
			Pair.<String, String>of(" UserController\\.getUserNotificationsPage", " (/*TODO play25 migration*/(UserController)null).getUserNotificationsPage"),
			Pair.<String, String>of(" ReportController\\.showReport", " (/*TODO play25 migration*/(ReportController)null).showReport"),
			
			Pair.<String, String>of(" AutomatedTaskController\\.updateCache", " (/*TODO play25 migration*/(AutomatedTaskController)null).updateCache"),
			
			Pair.<String, String>of(" AutomatedTaskController\\.updateCache", " (/*TODO play25 migration*/(AutomatedTaskController)null).updateCache"),
	};
		
	public CallControllerStaticModifier() {
		super(Arrays.asList(REPLACEMENTS));
		Arrays.asList(REPLACEMENTS).stream().map(Pair::getLeft).map(s -> s.replace("\\", "")).forEach(System.out::println);
	}

	@Override
	public boolean isApplied(File file) {
		return file.getName().endsWith(".java");
	}

}
