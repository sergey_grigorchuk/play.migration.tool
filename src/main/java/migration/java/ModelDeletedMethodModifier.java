package migration.java;

import java.io.File;
import java.util.List;

import migration.JavaTokenizerModifier;
import migration.parser.JavaParserConstants;
import migration.parser.Token;

public class ModelDeletedMethodModifier extends JavaTokenizerModifier {

	@Override
	protected List<Token> processTokenList(File file, List<Token> tokens) {
		int extIdx = findNextToken(tokens, 0, JavaParserConstants.EXTENDS);
		if(extIdx > 0) {
			// Нашли extends
			Token className = getNextSignificantToken(tokens, extIdx + 1);
			if(className != null && (className.image.equals("Model") 
					|| className.image.equals("RefbookModel")
					|| className.image.equals("ChangeLoggedModel")
					)) {
				// Наследник Model
				// Ищем метод delete
				int deleteIdx = findMethodDeclaration(tokens, "delete");
				if(deleteIdx >= 0) {
					Token voidToken = getNextSignificantToken(tokens, deleteIdx + 1);
					if(voidToken != null && voidToken.kind == JavaParserConstants.VOID) {
						voidToken.image = "boolean";
						int endOfDeleteIdx = findEndOfBlock(tokens, deleteIdx);
						int superDeleteIdx = findNextToken(tokens, deleteIdx, JavaParserConstants.SUPER);
						if(superDeleteIdx < endOfDeleteIdx && superDeleteIdx >= 0) {
							Token superDeleteToken = tokens.get(superDeleteIdx);
							superDeleteToken.image = "return super";
						} else {
							int ebeanDeleteIdx = findNextToken(tokens, deleteIdx, "EbeanService");
							Token ebeanDeleteToken = tokens.get(ebeanDeleteIdx);
							ebeanDeleteToken.image = "return EbeanService";
							/*
							if(ebeanDeleteIdx < endOfDeleteIdx && ebeanDeleteIdx >= 0) {
								int semiColonIdx = findNextToken(tokens, ebeanDeleteIdx, JavaParserConstants.SEMICOLON);
								if(semiColonIdx >= 0  && semiColonIdx < endOfDeleteIdx) {
									Token semicolonToken = tokens.get(semiColonIdx);
									semicolonToken.image = ";\nreturn true;";
								}
							}
							*/
						}
					}
				}
			}
		}
		return tokens;
	}

	/*
	@Override
	public boolean isApplied(File file) {
		return file.getName().equals("Region.java");
	}
	//*/
	
	

}
