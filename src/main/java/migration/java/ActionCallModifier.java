package migration.java;

import java.io.File;
import java.util.List;

import migration.JavaTokenizerModifier;
import migration.parser.JavaParserConstants;
import migration.parser.Token;

public class ActionCallModifier extends JavaTokenizerModifier {

	@Override
	protected List<Token> processTokenList(File file, List<Token> tokens) {
		int callIdx = findMethodDeclaration(tokens, "call");
		if(callIdx >= 0) {
			int idx = callIdx;
			while(idx < tokens.size()) {
				idx++;
				Token t = tokens.get(idx);
				if(t.kind == JavaParserConstants.THROWS) {
					Token nextT = getNextSignificantToken(tokens, idx + 1);
					if(nextT.image.equals("Throwable")) {
						t.image = "";
						nextT.image = "";
						break;
					}
				}
				if(t.kind == JavaParserConstants.RBRACE) {
					break;
				}
			}
		}
		return tokens;
	}

}
