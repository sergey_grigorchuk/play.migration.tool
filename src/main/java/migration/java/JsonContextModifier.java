package migration.java;

import java.io.File;
import java.util.Arrays;

import org.apache.commons.lang3.tuple.Pair;

import migration.RegexpModifier;

public class JsonContextModifier extends RegexpModifier {

	public static Pair<String, String>[] REPLACEMENTS = new Pair[] {
			Pair.<String, String>of("Ebean\\.createJsonContext\\(\\)\\.toJsonString\\(deliveryItems\\, false", "Ebean.json().toJson(deliveryItems"),
	};
		
	public JsonContextModifier() {
		super(Arrays.asList(REPLACEMENTS));
	}

	@Override
	public boolean isApplied(File file) {
		return file.getName().endsWith(".java");
	}

}
