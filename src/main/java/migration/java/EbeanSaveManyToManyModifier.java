package migration.java;

import java.io.File;
import java.util.List;

import migration.JavaTokenizerModifier;
import migration.parser.JavaParserConstants;
import migration.parser.Token;

public class EbeanSaveManyToManyModifier extends JavaTokenizerModifier {

	@Override
	protected List<Token> processTokenList(File file, List<Token> tokens) {
		int idx = 0;
		while(idx >= 0) {
			idx = findNextToken(tokens, idx + 1, "saveManyToManyAssociations");
			if(idx >= 0) {
				Token bean = tokens.get(idx - 2);
				String beanName = bean.image;
				if(bean.kind == JavaParserConstants.IDENTIFIER && !beanName.equals("Ebean")) {
					Token saveM2M = tokens.get(idx);
					Token dot = tokens.get(idx - 1);
					Token bracket = tokens.get(idx + 1);
					bean.image = "com.avaje.ebean.Ebean.saveManyToManyAssociations";
					dot.image = "(";
					saveM2M.image = beanName;
					bracket.image = ", ";
				}
			}
		}
		return tokens;
	}

}
