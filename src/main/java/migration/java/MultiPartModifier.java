package migration.java;

import java.io.File;
import java.util.Arrays;

import org.apache.commons.lang3.tuple.Pair;

import migration.RegexpModifier;

public class MultiPartModifier extends RegexpModifier {

	public static Pair<String, String>[] REPLACEMENTS = new Pair[] {
			Pair.<String, String>of("MultipartFormData ", "MultipartFormData<java.io.File> "),
			Pair.<String, String>of("MultipartFormData\\.FilePart ", "MultipartFormData.FilePart<java.io.File> "),
	};
		
	public MultiPartModifier() {
		super(Arrays.asList(REPLACEMENTS));
	}

	@Override
	public boolean isApplied(File file) {
		return file.getName().endsWith(".java");
	}

}
