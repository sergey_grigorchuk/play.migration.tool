package migration.java;

import java.io.File;
import java.util.Arrays;

import org.apache.commons.lang3.tuple.Pair;

import migration.RegexpModifier;

public class RouterModifier extends RegexpModifier{
	public static Pair<String, String>[] REPLACEMENTS = new Pair[] {
			Pair.<String, String>of("import play\\.core\\.Router;", "import play.api.routing.JavaScriptReverseRoute;import play.routing.JavaScriptReverseRouter;"),
			Pair.<String, String>of("Router.JavascriptReverseRoute", "JavaScriptReverseRoute"),
			// Pair.<String, String>of("Router.JavascriptReverseRouter", "JavaScriptReverseRoute"),
		};
		
		public RouterModifier() {
			super(Arrays.asList(REPLACEMENTS));
		}

		@Override
		public boolean isApplied(File file) {
			return file.getName().endsWith(".java");
		}
}
