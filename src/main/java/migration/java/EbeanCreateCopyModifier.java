package migration.java;

import java.io.File;
import java.util.List;

import migration.JavaTokenizerModifier;
import migration.parser.Token;

public class EbeanCreateCopyModifier extends JavaTokenizerModifier {

	@Override
	protected List<Token> processTokenList(File file, List<Token> tokens) {
		int idx = 0;
		while(idx >= 0) {
			idx = findNextToken(tokens, idx + 1, "_ebean_createCopy");
			if(idx >= 0) {
				Token bean = tokens.get(idx - 2);
				String beanName = bean.image;
				if(!beanName.equals("Ebean")) {
					Token saveM2M = tokens.get(idx);
					Token dot = tokens.get(idx - 1);
					Token bracket = tokens.get(idx + 1);
					bean.image = "services.core.EbeanService._ebean_createCopy";
					dot.image = "(";
					saveM2M.image = beanName;
					bracket.image = "";
				}
			}
		}
		return tokens;
	}

	@Override
	public boolean isApplied(File file) {
		return super.isApplied(file) && !file.getName().equals("EbeanService.java");
	}
	
	

}
