package migration.java;

import java.io.File;
import java.util.Arrays;

import org.apache.commons.lang3.tuple.Pair;

import migration.RegexpModifier;

public class ImportEbeanModelModifier extends RegexpModifier{
	public static Pair<String, String>[] REPLACEMENTS = new Pair[] {
		// Pair.<String, String>of("import com\\.avaje\\.ebean\\.event\\.", "import io.ebean.event."),
		Pair.<String, String>of("import play\\.db\\.ebean\\.Model", "import com.avaje.ebean.Model"),
		Pair.<String, String>of("import static play\\.db\\.ebean\\.Model", "import static com.avaje.ebean.Model")
	};
	
	public ImportEbeanModelModifier() {
		super(Arrays.asList(REPLACEMENTS));
	}

	@Override
	public boolean isApplied(File file) {
		return file.getName().endsWith(".java"); // && file.getAbsolutePath().contains("/models/");
	}

}
