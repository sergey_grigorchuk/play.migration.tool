package migration.java;

import java.io.File;
import java.util.List;

import migration.JavaTokenizerModifier;
import migration.parser.JavaParserConstants;
import migration.parser.Token;

public class PersistListenerModifier extends JavaTokenizerModifier {

	@Override
	protected List<Token> processTokenList(File file, List<Token> tokens) {
		// Убираем PersistListenerAdapter<T>
		int plaIdx = 0;
		while(plaIdx >= 0) {
			plaIdx = findNextToken(tokens, plaIdx + 1, "PersistListenerAdapter");
			if(plaIdx > 0) {
				if(tokens.get(plaIdx + 1).kind == JavaParserConstants.LT) {
					tokens.remove(plaIdx + 1);
					tokens.remove(plaIdx + 1);
					tokens.remove(plaIdx + 1);

					plaIdx = findNextToken(tokens, plaIdx + 1, "BeanPersistListener");
					if(plaIdx > 0) {
						if(tokens.get(plaIdx + 1).kind == JavaParserConstants.LT) {
							tokens.remove(plaIdx + 1);
							tokens.remove(plaIdx + 1);
							tokens.remove(plaIdx + 1);
						}
					}

				}
			}
		}
		
		{
			int idx = findMethodDeclaration(tokens, "doRecalc");
			if(idx > 0) {
				int parenIdx = findNextToken(tokens, idx + 1, JavaParserConstants.LPAREN);
				int typeIdx = findNextToken(tokens, parenIdx + 1, JavaParserConstants.IDENTIFIER);
				tokens.get(typeIdx).image = "Object";
				int argIdx = findNextToken(tokens, typeIdx + 1, JavaParserConstants.IDENTIFIER);
				tokens.get(argIdx).image = "_bean";
				int beanIdx = findNextToken(tokens, argIdx + 1, JavaParserConstants.IDENTIFIER);
				int typeNameIdx = findNextToken(tokens, beanIdx + 1, JavaParserConstants.IDENTIFIER);
				Token typeName = tokens.get(typeNameIdx);
				Token beanDecl = tokens.get(beanIdx);
				beanDecl.image = typeName.image + " " + beanDecl.image; 
	
				int _beanIdx = findNextToken(tokens, argIdx + 1, "bean");
				Token _beanToken = tokens.get(_beanIdx);
				_beanToken.image = "((" + typeName + ")_bean)";
			}
		}
		int idx = findMethodDeclaration(tokens, "deleted");
		if(idx > 0) {
			int parenIdx = findNextToken(tokens, idx + 1, JavaParserConstants.LPAREN);
			int typeIdx = findNextToken(tokens, parenIdx + 1, JavaParserConstants.IDENTIFIER);
			Token typeToken = tokens.get(typeIdx);
			Token beanToken = getNextSignificantToken(tokens, typeIdx + 1);
			beanToken.image = "_bean";
			String typeName = typeToken.image;
			typeToken.image = "Object";
			int argIdx = findNextToken(tokens, typeIdx + 1, JavaParserConstants.LBRACE);
			tokens.get(argIdx).image = "{ " + typeName + " bean = (" + typeName + ")_bean;";
		}
		
		return tokens;
	}

	@Override
	public boolean isApplied(File file) {
		return file.getName().endsWith("PersistListener.java");
	}

}
