package migration.java;

import java.io.File;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import migration.JavaTokenizerModifier;
import migration.parser.JavaParserConstants;
import migration.parser.Token;

public class StaticControllerMethodModifier extends JavaTokenizerModifier {

	private static Set<String> FILTER = new HashSet<String>(Arrays.asList(new String[]{
		"getCurrentUser",
		"getCurrentUserFromSession",
		"getCurrentUserFromRequest",
		"getCurrentUserEmail",
		"getRecords",
		"getRecord",
		"updateRecord",
		"createRecord",
		"deleteRecord",
		"getCurrentUserEmail",
		"getCurrentHelpUrl"
	}));
	
	@Override
	protected List<Token> processTokenList(File file, List<Token> tokens) {
		if(file.getName().equals("Assets.java")) {
			System.currentTimeMillis();
		}
		for(int i = tokens.size() - 1; i >= 0; i--) {
			Token t = tokens.get(i);
			if(t.kind == JavaParserConstants.STATIC) { //  Находим статические методы
				// Проверяем что возвращает Result
				Token returnType = getNextSignificantToken(tokens, i + 1);
				if(returnType.image.equals("Result") || returnType.image.equals("Action") || returnType.image.equals("WebSocket")) {
				
					// Проверяем на константы
					Token methodName = findMethodName(tokens, i);
					
					if(methodName != null) {
						if(!FILTER.contains(methodName.image) && 
								(!file.getName().equals("RefbookEditorController.java") && !methodName.image.equals("render"))
								) { // Отфильтровываем часть методов
							tokens.remove(i);
						}
					}
				}
			}
			if(t.kind == JavaParserConstants.PUBLIC) { //  Находим объявление класса
				Token next = getNextSignificantToken(tokens, i + 1);
				if(next.kind == JavaParserConstants.CLASS) {
					t.image = "@com.google.inject.Singleton\n" + t.image;
				}
			}
		}
		return tokens;
	}

	@Override
	public boolean isApplied(File file) {
		return file.getName().endsWith(".java") && (file.getName().contains("Controller") 
				|| file.getName().equals("Assets.java")
				|| file.getName().equals("Index.java")
				);
	}

	
}
