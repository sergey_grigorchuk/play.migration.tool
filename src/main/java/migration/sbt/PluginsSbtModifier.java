package migration.sbt;

import java.io.File;
import java.util.Arrays;

import org.apache.commons.lang3.tuple.Pair;

import migration.RegexpModifier;

public class PluginsSbtModifier extends RegexpModifier{
	public static Pair<String, String>[] REPLACEMENTS = new Pair[] {
		Pair.<String, String>of("\"sbt-plugin\" % \"([^\"]+)\"\\)", "\"sbt-plugin\" % \"2.5.15\")\naddSbtPlugin(\"com.typesafe.sbt\" % \"sbt-play-ebean\" % \"3.0.2\")\naddSbtPlugin(\"com.typesafe.sbteclipse\" % \"sbteclipse-plugin\" % \"4.0.0\")\n"),
	};
	
	public PluginsSbtModifier() {
		super(Arrays.asList(REPLACEMENTS));
	}

	@Override
	public boolean isApplied(File file) {
		return file.getName().equals("plugins.sbt");
	}

}
