package migration.sbt;

import java.io.File;
import java.util.Arrays;

import org.apache.commons.lang3.tuple.Pair;

import migration.RegexpModifier;

public class BuildSbtModifier extends RegexpModifier{
	public static Pair<String, String>[] REPLACEMENTS = new Pair[] {
		Pair.<String, String>of("javaEbean,", "// javaEbean,"),
		// Pair.<String, String>of("scalaVersion", "// scalaVersion"),
		Pair.<String, String>of("enablePlugins\\(PlayJava\\)", "enablePlugins(PlayJava, PlayEbean)"),
		Pair.<String, String>of("scalaVersion := \"2.11.1\"", "scalaVersion := \"2.11.11\""),
		Pair.<String, String>of("maintainer", "//maintainer"),
		Pair.<String, String>of("packageSummary", "//packageSummary"),
		Pair.<String, String>of("packageDescription", "//packageDescription"),
		Pair.<String, String>of("serverLoading", "//serverLoading"),
		Pair.<String, String>of("debianPackageDependencies[^(]+\\(([^)]|\\n|\\r)+\\)", "//debianPackageDependencies")
	};
	
	public BuildSbtModifier() {
		super(Arrays.asList(REPLACEMENTS));
	}

	@Override
	public boolean isApplied(File file) {
		return file.getName().equals("build.sbt");
	}

}
