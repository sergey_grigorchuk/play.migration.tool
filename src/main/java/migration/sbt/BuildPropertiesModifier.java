package migration.sbt;

import java.io.File;
import java.util.Arrays;

import org.apache.commons.lang3.tuple.Pair;

import migration.RegexpModifier;

public class BuildPropertiesModifier extends RegexpModifier{
	public static Pair<String, String>[] REPLACEMENTS = new Pair[] {
		Pair.<String, String>of("sbt.version=.+", "sbt.version=0.13.15"),
	};
	
	public BuildPropertiesModifier() {
		super(Arrays.asList(REPLACEMENTS));
	}

	@Override
	public boolean isApplied(File file) {
		return file.getName().equals("build.properties");
	}

}
