package migration.scala.html;

import java.io.File;
import java.util.Arrays;

import org.apache.commons.lang3.tuple.Pair;

import migration.RegexpModifier;

public class GetInstanceModifier extends RegexpModifier{

	public static Pair<String, String>[] REPLACEMENTS = new Pair[] {
			Pair.<String, String>of("\\.getInstance\\(\\)\\.", ".getInstance."),
	};

	public GetInstanceModifier() {
		super(Arrays.asList(REPLACEMENTS));
	}

	@Override
	public boolean isApplied(File file) {
		return file.getName().endsWith(".scala.html");
	}

}
