package migration;

import java.io.File;

/**
 * Интерфейс для универсального модификатора, который делает одно типовое изменение кода
 * @author ukman
 *
 */
public interface SrcModifier {
	/**
	 * Отвечает на вопрос, можно ли с помощью этого модификатора обрабатывать этот файл
	 * @param file
	 * @return
	 */
	boolean isApplied(File file);
	
	/**
	 * Меняет content файла
	 * @param content
	 * @return
	 */
	String modify(File file, String content);
}
