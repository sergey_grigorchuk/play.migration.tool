package migration;

import java.io.File;
import java.util.List;

import org.apache.commons.lang3.tuple.Pair;

import lombok.RequiredArgsConstructor;

/**
 * Изменяет строку по списку пар regexp-ов шаблон-замена
 * @author ukman
 *
 */
@RequiredArgsConstructor
public abstract class RegexpModifier implements SrcModifier{
	private final List<Pair<String, String>> replacements;

	@Override
	public String modify(File file, String content) {
		String res = content;
		for(Pair<String, String> replacement : replacements) {
			res = res.replaceAll(replacement.getLeft(), replacement.getRight());
		}
		return res;
	}
	
}
