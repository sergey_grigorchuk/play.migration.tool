package migration;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Collection;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.IOFileFilter;
import org.apache.commons.io.filefilter.NameFileFilter;
import org.apache.commons.io.filefilter.NotFileFilter;
import org.apache.commons.io.filefilter.TrueFileFilter;

import migration.conf.ConfModifier;
import migration.java.ActionCallModifier;
import migration.java.ActionModifier;
import migration.java.CallControllerStaticModifier;
import migration.java.EbeanCreateCopyModifier;
import migration.java.EbeanSaveManyToManyModifier;
import migration.java.EbeanUpdateModifier;
import migration.java.I18NModifier;
import migration.java.ImportEbeanModelModifier;
import migration.java.JsonContextModifier;
import migration.java.ModelDeletedMethodModifier;
import migration.java.MultiPartModifier;
import migration.java.PersistListenerModifier;
import migration.java.RouterModifier;
import migration.java.StaticControllerMethodModifier;
import migration.java.WebSocketModifier;
import migration.sbt.BuildPropertiesModifier;
import migration.sbt.BuildSbtModifier;
import migration.sbt.PluginsSbtModifier;
import migration.scala.html.GetInstanceModifier;

public class Main {
	
	public static SrcModifier[] MODIFIERS = new SrcModifier[]{
		new BuildSbtModifier(),
		new BuildPropertiesModifier(),
		new PluginsSbtModifier(),
		new ImportEbeanModelModifier(),
		new StaticControllerMethodModifier(),
		new GetInstanceModifier(),
		new PersistListenerModifier(),
		new ModelDeletedMethodModifier(),
		new EbeanUpdateModifier(),
		new EbeanSaveManyToManyModifier(),
		new EbeanCreateCopyModifier(),
		new CallControllerStaticModifier(),
		new JsonContextModifier(),
		new MultiPartModifier(),
		new ActionCallModifier(),
		new I18NModifier(),
		new ActionModifier(),
		new WebSocketModifier(),
		new RouterModifier(),
		new ConfModifier(),
	};
	
	public static void main(String args[]) throws IOException {
		if(args.length < 2) {
			System.out.println("Для запуска программы укажите папку с исходниками и папку куда поместить результат.");
			return;
		}
		
		File srcDir = new File(args[0]);
		File destDir = new File(args[1]);

		long t1 = System.currentTimeMillis();
		
		// Получаем список файлов для изменения
		IOFileFilter dirFilter = new NotFileFilter(new NameFileFilter(new String[]{"target", ".git"}));
		Collection<File> srcFiles = FileUtils.listFiles(srcDir , TrueFileFilter.INSTANCE, dirFilter);
		System.out.println("Found " + srcFiles.size() +  " file(s)");
		
		for(File file : srcFiles) {
			
			// Подготавливаем папку в результирующем месте
			String relPath = file.getAbsolutePath().substring(srcDir.getAbsolutePath().length());
			File destFile = new File(destDir, relPath);
			destFile.getParentFile().mkdirs();
			
			// Читаем содержимое
			String content = FileUtils.readFileToString(file, StandardCharsets.UTF_8);
			
			// Пропускаем Через модификаторы
			for(SrcModifier modifier : MODIFIERS) {
				if(modifier.isApplied(file)) {
					content = modifier.modify(file, content);
				}
			}
			
			// ПРоверяем если файл не изменился
			String oldContent = null;
			if(destFile.exists()) {
				oldContent = FileUtils.readFileToString(destFile);
			}
			// Записываем на диск 
			if(!content.equals(oldContent)) {
				FileUtils.write(destFile, content, StandardCharsets.UTF_8);
			}

		}
		
		long t = System.currentTimeMillis() - t1;
		System.out.println("\n\nProcessing time : " + t);
	}
}
