package migration;

import java.io.File;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import migration.parser.JavaCharStream;
import migration.parser.JavaParserConstants;
import migration.parser.JavaParserTokenManager;
import migration.parser.Token;

/**
 * Базовый класс для модификаторов Java файлов по токенам. 
 * Каждый наследник должен реализовать метод #processTokenList
 * @author ukman
 *
 */
public abstract class JavaTokenizerModifier implements SrcModifier {

	@Override
	public boolean isApplied(File file) {
		return file.getName().endsWith(".java");
	}

	@Override
	public String modify(File file, String content) {
		JavaCharStream stream = new JavaCharStream(new StringReader(content));
		JavaParserTokenManager tokenManager = new JavaParserTokenManager(stream);
		Token token = tokenManager.getNextToken();
		List<Token> tokens = new ArrayList<Token>();
		
		while(token != null && token.kind != 0) {
			tokens.add(token);
			token = tokenManager.getNextToken();
		}
		
		tokens = processTokenList(file, tokens);
		StringBuilder res = new StringBuilder();
		for(Token t : tokens) {
			res.append(t.image);
		}

		return res.toString();
	}

	/**
	 * Метод получает список токенов из Java исходника и должен вернуть новый список токенов.
	 * В токене нужно проставить поле image- текстовое содержимое, остальное не важно.
	 * @param tokens Список токенов
	 * @return Новый список токенов (содержимое)
	 */
	protected abstract List<Token> processTokenList(File file, List<Token> tokens);

	/**
	 * Ищем имя метода по правилу- после имени метода должна быть открывающая круглая скобка
	 * @param i
	 * @return
	 */
	protected Token findMethodName(List<Token> tokens, int i) {
		Token prevToken = tokens.get(i);
		
		while(i < tokens.size()) {
			i++;
			Token t = tokens.get(i);
			if(t.kind == JavaParserConstants.LPAREN) {
				return prevToken;
			}
			if(t.kind == JavaParserConstants.SEMICOLON || t.kind == JavaParserConstants.LBRACE) {
				// Нашли точку ; или {- значит метода не было
				return null;
			}
			if(t.kind == JavaParserConstants.IDENTIFIER) {
				prevToken = t;
			}
		}
		return null;
	}
	
	/**
	 * Ищем объявление метода по признаку (public|private|protected) XXX+ methodName(XXX+) {
	 * @param tokens
	 * @param methodName
	 * @return
	 */
	protected int findMethodDeclaration(List<Token> tokens, String methodName) {
		int res = -1;
		int methodIdx = 0;
		while(methodIdx >= 0) {
			methodIdx = findNextTokens(tokens, methodIdx + 1, new HashSet(Arrays.asList(new String[]{"protected", "public", "private"})));
			if(methodIdx >= 0) {
				Token foundMethodName = findMethodName(tokens, methodIdx);
				if(foundMethodName != null && foundMethodName.image.equals(methodName)) {
					return methodIdx;
				}
			}
		}
		return res;
	}
	
	protected int findEndOfBlock(List<Token> tokens, int beforeBlockIdx) {
		int idx = beforeBlockIdx;
		// Ищем открывающую скобку
		while(idx < tokens.size()) {
			Token t = tokens.get(idx);
			if(t.kind == JavaParserConstants.LBRACE) {
				break;
			}
			idx++;
		}
		// Ищем закрывающую скобку
		int level = 1;
		while(level > 0) {
			Token t = tokens.get(idx);
			if(t.kind == JavaParserConstants.LBRACE) {
				level++;
			} else if(t.kind == JavaParserConstants.RBRACE) {
				level--;
			}
			idx++;
		}
		return idx;
	}
	
	protected int findNextToken(List<Token> tokens, int idx, String image) {
		return findNextTokens(tokens, idx, new HashSet(Arrays.asList(new String[]{image})));
	}
	
	protected int findNextTokens(List<Token> tokens, int idx, Set<String> images) {
		while(idx < tokens.size()) {
			Token t = tokens.get(idx);
			if(images.contains(t.image)) {
				return idx;
			}
			idx++;
		}
		return -1;
	}
	
	protected int findNextToken(List<Token> tokens, int idx, int kind) {
		while(idx < tokens.size()) {
			Token t = tokens.get(idx);
			if(t.kind == kind) {
				return idx;
			}
			idx++;
		}
		return -1;
	}
	
	protected Token getNextSignificantToken(List<Token> tokens, int idx) {
		int resIdx = getNextSignificantTokenIdx(tokens, idx);
		if(resIdx >= 0) {
			return tokens.get(resIdx);
		}
		return null;
	}
	
	protected int getNextSignificantTokenIdx(List<Token> tokens, int idx) {
		while(idx < tokens.size()) {
			Token t = tokens.get(idx);
			if(isSignificant(t)) {
				return idx;
			}
			idx++;
		}
		return -1;
	}
	
	protected boolean isSignificant(Token t) {
		return t.kind != JavaParserConstants.SPACE &&
				 t.kind != JavaParserConstants.SINGLE_LINE_COMMENT &&
				 t.kind != JavaParserConstants.MULTI_LINE_COMMENT &&
				 t.kind != JavaParserConstants.RETURN;
	}
}
